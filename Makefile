sites = $(patsubst data/%, %, $(wildcard data/*))
check_all = $(patsubst %, check-%, $(sites))

all: check build

build:
	cd site && nanoc

check: $(check_all)

$(check_all): check-% : data/%
	@./scripts/quiet-test $*

deploy: check build
	cd site && nanoc deploy
	git push

pull-deploy:
	git pull
	$(MAKE) deploy
