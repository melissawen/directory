# About the Noosfero directory

The Noosfero directory is a list of sites running Noosfero. It has two main
purposes:

* To serve as a showcase of work done with Noosfero
* To be a central hub for discovering Noosfero sites when we finally add for
  federation.

## API

For now, a [JSON file](/all.json) is available with data from all the sites.
The format should be self-explanatory and will not change, apart from adding
new fields or metadata.

## Adding a new site

<a name="new-entry"></a>

### Creating an entry

An entry on the directory is a directory named after the site, containing only
lowercase letters (`a-z`), numbers (`0-9`), '`_`' (underscore) and '`-`'
(hiphen). This directory must have the following files:

* `screenshot.png`: must be a **1024x768 PNG image** (mandatory)
* `data.yaml`: a YAML data file. It must be encoded in UTF-8, and contain at
  least the following fields:
  * `name`: Name of the site (mandatory)
  * `url`: public url of the site (mandatory)

On the directory repository, all entries will be stored under the `data/`
directory. Example:

~~~
data/softwarelivre/
├── data.yaml
└── screenshot.png
~~~

The contents of `data/softwarelivre/data.yaml` looks like this:

~~~
name: Software Livre Brasil
url: http://softwarelivre.org/
~~~

### Submitting your entry

There are two ways to submit an entry:

* If you are confortable with gittlab and git, you can send a merge request
  against the [repository on Gitlab](https://gitlab.com/noosfero/directory/).
  The merge request must include the entry inside the `data/` directory, just
  [like the others](https://gitlab.com/noosfero/directory/tree/master).
  This is the preferred method.

* If you are not comfortable with using git, you can also compact your entry
  (e.g. as `.tar.gz` or `.zip`), create an
  [an issue](https://gitlab.com/noosfero/directory/issues) on Gitlab and attach
  your compacted file to the issue.
