if [ -z "$SITE" ]; then
  echo "E: SITE is undefined"
  exit 1
fi

DATA="$SITE/data.yaml"

test_name() {
  assertTrue "$DATA does not provide a name!" "grep -q ^name: $DATA"
}

test_url() {
  assertTrue "$DATA does not provide a url!" "grep -q ^name: $DATA"
}

test_screenshot() {
  local screenshot="$SITE/screenshot.png"
  assertEquals "$screenshot is not 1024x768" "1024x768" "$(identify "$screenshot" | awk '{print($3)}')"
}

test_encoding() {
  local encoding=$(file --brief --mime-encoding "$DATA")
  assertTrue "$DATA is not encoded in UTF-8!" "[ $encoding = utf-8 -o $encoding = us-ascii ]"
}

. shunit2
