class CreateThumbnail < Nanoc::Filter

  identifier :create_thumbnail
  type :binary

  def run(filename, params)
    system(
      'convert',
      '-resize',
      params[:width].to_s,
      filename,
      output_filename
    )
  end

end
