#!/usr/bin/env ruby

# A few helpful tips about the Rules file:
#
# * The string given to #compile and #route are matching patterns for
#   identifiers--not for paths. Therefore, you can’t match on extension.
#
# * The order of rules is important: for each item, only the first matching
#   rule is applied.
#
# * Item identifiers start and end with a slash (e.g. “/about/” for the file
#   “content/about.html”). To select all children, grandchildren, … of an
#   item, use the pattern “/about/*/”; “/about/*” will also select the parent,
#   because “*” matches zero or more characters.

route '/sites/*/data/' do
  item.identifier.gsub(%r{data/}, '') + 'index.html'
end

route '/htaccess/' do
  '/.htaccess'
end

compile '*' do
  if item[:extension] == 'css' || item[:extension] == 'js'
    # don’t filter stylesheets/javascript
  elsif item[:extension] == 'json.erb'
    filter :erb
  elsif item.binary?
    # don’t filter binary items
  else
    filter :erb
    if item[:extension] == 'md'
      filter :kramdown
    end
    layout 'default'
    filter :relativize_paths, :type => :html
  end
end

route '*' do
  if item[:extension] == 'css' || item[:extension] == 'js'
    # Write item with identifier /foo/ to /foo.css
    item.identifier.chop + '.' + item[:extension]
  elsif item[:extension] == 'json.erb'
    item.identifier.chop + '.json'
  elsif item.binary?
    # Write item with identifier /foo/ to /foo.ext
    item.identifier.chop + (item[:extension] ? '.' + item[:extension] : '')
  else
    # Write item with identifier /foo/ to /foo/index.html
    item.identifier + 'index.html'
  end
end

compile '/sites/*/screenshot/', :rep => :thumb do
  filter :create_thumbnail, :width => 240
end

route '/sites/*/screenshot/', :rep => :thumb do
  item.identifier.chop + ".thumb.png"
end

layout '*', :erb
